package com.example.hellojava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btOK;
    EditText editNama;
    TextView hasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btOK = (Button) findViewById(R.id.button);
        editNama= (EditText) findViewById(R.id.namaPeserta);
        hasil = (TextView) findViewById(R.id.dataPeserta);

        btOK.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                hasil.setText("Hello " + editNama.getText().toString() + "!\n Peserta VSGA" +"");
            }
        });
    }
}
