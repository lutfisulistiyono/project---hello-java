package com.example.hellojava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class kalkulator extends AppCompatActivity {

    Button btn;
    EditText input1,input2;
    TextView hasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalkulator);
        btn= (Button) findViewById(R.id.button);
        input1= (EditText) findViewById(R.id.editText);
        input2= (EditText) findViewById(R.id.editText2);
        hasil = (TextView) findViewById(R.id.textView3);
        btn.setOnClickListener(new Button.OnClickListener(){
                 @Override
                public void onClick(View v){
                   // hasil.setText("Hello " + editNama.getText().toString() + "!\n Peserta VSGA" +"");
                    int inpt1 = Integer.parseInt(input1.getText().toString());
                    int inpt2 = Integer.parseInt(input2.getText().toString());
                    int hsl = inpt1 + inpt2;
                    hasil.setText(Integer.toString(hsl));
                }
        });
    }
}
