package com.example.hellojava;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class GanjilGenapActivity extends AppCompatActivity {
    EditText input;
    TextView hasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganjil_genap);
         input = (EditText)findViewById(R.id.inputBilangan);
         hasil = (TextView) findViewById(R.id.hasil);
        input.setOnEditorActionListener(new EditText.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE || i == KeyEvent.KEYCODE_BACK || (keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER))){
                    if (Integer.parseInt(input.getText().toString()) % 2 == 1){
                        hasil.setVisibility(View.VISIBLE);
                        hasil.setText("GANJIL !!!");
                    }else{
                        hasil.setText("GENAP !!!");
                    }
                }
                return false;
            }

        });
    }
}
