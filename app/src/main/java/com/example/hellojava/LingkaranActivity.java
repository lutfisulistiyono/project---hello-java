package com.example.hellojava;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class LingkaranActivity extends AppCompatActivity {

    //Button btn;
    EditText jari2;
    TextView luas,keliling;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lingkaran);
        jari2 = (EditText)findViewById(R.id.inputjari2);
        luas = (TextView) findViewById(R.id.luas);
        keliling = (TextView) findViewById(R.id.keliling);
        jari2.setOnEditorActionListener(new EditText.OnEditorActionListener(){
            @Override
            public boolean onEditorAction (TextView v, int actionId, KeyEvent event){
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == KeyEvent.KEYCODE_BACK || (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))){
                    String jari = jari2.getText().toString();
                    double HslLuas = Math.pow(Double.parseDouble(jari), 2)  * 3.14 ;
                    double HslKeliling = 2 * 3.14 * Double.parseDouble(jari);
                    luas.setVisibility(View.VISIBLE) ;
                    keliling.setVisibility(View.VISIBLE) ;
                    luas.setText("Luas: "+ HslLuas );
                    keliling.setText("Keliling : "+ HslKeliling );

                }
                return false;
            }

        });
    }
}
