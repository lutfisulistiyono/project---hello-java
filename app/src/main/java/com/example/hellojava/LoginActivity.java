package com.example.hellojava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class LoginActivity extends AppCompatActivity {
    Button btn_login;
    EditText txt_username, txt_password;
    public  String txtpass ="" ;
    String txtuser ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btn_login = (Button) findViewById(R.id.button3);
        txt_password = (EditText) findViewById(R.id.editText3);
        txt_username = (EditText) findViewById(R.id.editText4);

         btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtpass = txt_password.getText().toString();
                txtuser = txt_password.getText().toString();
            //if(validate()) {
                if (LoginActivity.this.txtpass.equals(LoginActivity.this.txtuser)) {
                    Intent login_intent = new Intent(LoginActivity.this, MenuActivity.class);
                    startActivity(login_intent);
                    Toast.makeText(LoginActivity.this, "This is my Toast message!" + txtpass,
                            Toast.LENGTH_LONG).show();
                }
           // }
            }
        });
    }

    public boolean validate() {
        boolean valid = false;

        //Get values from EditText fields
        String Email = txt_username.getText().toString();
        String Password = txt_password.getText().toString();

        //Handling validation for Email field
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
            Toast.makeText(LoginActivity.this, "This is my Toast message!",
            Toast.LENGTH_LONG).show();
        }else {
            valid = true;
        }
        //Handling validation for Password field
       if (Password.isEmpty()) {
           Toast.makeText(LoginActivity.this, "This is my Toast message!",
                   Toast.LENGTH_LONG).show();
        }else {
           valid = true;
       }

        return valid;
    }


}
